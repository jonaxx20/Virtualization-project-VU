#!/bin/bash

echo "Telemundo presento!"
USER=
PASS=
ENDPOINT=https://grid5.mif.vu.lt/cloud3/RPC2
#echo setting locales
#locale-gen en_US.UTF-8 > /dev/null
echo "Getting ready..."
apt-get update -y
apt-get upgrade -y
#echo
#echo
#echo install opennebula-tools
apt-get -y install opennebula-tools
#echo install git
apt-get -y install git
#echo Creating Vms:
#echo
ID1="$(onetemplate instantiate "ubuntu-16.04PTM" --name webserver  --user $USER --password $PASS --endpoint $ENDPOINT  | cut -d " " -f 3)"
ID2="$(onetemplate instantiate "Copy of ubuntu-16.04PTMDB" --name database  --user $USER --password $PASS --endpoint $ENDPOINT  | cut -d " " -f 3)"
ID3="$(onetemplate instantiate "Copy of Copy of ubuntu-16.04PTMclient" --name client  --user $USER --password $PASS --endpoint $ENDPOINT  | cut -d " " -f 3)"



apt-get -y install software-properties-common
apt-add-repository -y ppa:ansible/ansible
apt-get update
apt-get -y install ansible
sleep 25
echo "Creating VMs"
echo $ID1
onevm show $ID1 --user $USER --password $PASS --endpoint $ENDPOINT
echo [webserver] > //etc/ansible/hosts
echo "$(onevm show $ID1 --user $USER --password $PASS --endpoint $ENDPOINT | grep -wi "PRIVATE_IP"|cut -d "\"" -f 2)" >> //etc/ansible/hosts
echo -n "$(onevm show $ID1 --user $USER --password $PASS --endpoint $ENDPOINT | grep -wi "PUBLIC_IP"|cut -d "\"" -f 2)" >> //etc/ansible/webIP
echo -n ":" >> //etc/ansible/webIP
echo -n "$(onevm show $ID1 --user $USER --password $PASS --endpoint $ENDPOINT | grep TCP\_PORT\_FORWARDING | grep -o '[0-9]\+:80' | sed 's/...$//')" >> //etc/ansible/webIP
IP="$(cat //etc/ansible/webIP)"
echo [database] >> //etc/ansible/hosts
echo "$(onevm show $ID2 --user $USER --password $PASS --endpoint $ENDPOINT | grep -wi "PRIVATE_IP"|cut -d "\"" -f 2)" >> //etc/ansible/hosts
echo [client] >> //etc/ansible/hosts
echo "$(onevm show $ID3 --user $USER --password $PASS --endpoint $ENDPOINT | grep -wi "PRIVATE_IP"|cut -d "\"" -f 2)" >> //etc/ansible/hosts
echo
sleep 15
cat /etc/ansible/hosts
echo
#ansible -m ping all
echo "downloading and excecuting ansible scripts"
cd /etc/ansible/
cat -n /etc/ansible/hosts |sed '2!d' | awk '{print $2}' | cat > web.txt
cat -n /etc/ansible/hosts |sed '4!d' | awk '{print $2}' | cat > mysql.txt
echo "StrictHostKeyChecking no" >> /etc/ssh/ssh_config
echo "UserKnownHostsFile=/dev/null" >> /etc/ssh/ssh_config
git clone https://gitlab.com/jonaxx20/Virtualization-project-VU
cd Virtualization-project-VU
sed -e "s/localhost/$IP/g" tele.sql > telef.sql
ANSIBLE_HOST_CHECKING_KEY=False ansible-playbook mysql.yml
ANSIBLE_HOST_CHECKING_KEY=False ansible-playbook  Webserver.yml
ANSIBLE_HOST_CHECKING_KEY=False ansible-playbook  client.yml
#ssh -o "StrictHostKeyChecking no" -A -p $(cat /Users/MAstudio/Desktop/port.txt) root@193.219.91.103 'bash -s' < /Users/MAstudio/Desktop/ansible.sh
