#!/bin/bash

echo -n "Enter the jumphost port:"
read port
echo PORT:$port
echo Making Connection
cd ~/.ssh
eval "$(ssh-agent -s)"
ssh-add ~/.ssh/id_rsa
ssh -A -p $port root@193.219.91.103 'bash -s' < /home/jonas/Workspace/Virtualization-project-VU/jumphost.sh

#- name: Extract WordPress
#  unarchive: src=/tmp/wordpress.tar.gz dest=/var/www/ copy=no
#  sudo: yes

  #- name: Download WordPress
  #  get_url:
  #     url=https://wordpress.org/latest.tar.gz
  #     dest=/tmp/wordpress.tar.gz
  #     validate_certs=no
